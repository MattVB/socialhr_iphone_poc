//
//  EmployeeSearchDataController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmployeeSearchDataController : NSObject

+(NSArray*)doEmployeeSearch:(NSString*)searchText;

@end
