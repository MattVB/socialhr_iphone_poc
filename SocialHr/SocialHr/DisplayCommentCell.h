//
//  DisplayCommentCell.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 7/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisplayCommentCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelCommenterName;
@property (weak, nonatomic) IBOutlet UILabel *labelComment;
@property (weak, nonatomic) IBOutlet UIImageView *imageCommenter;

@end
