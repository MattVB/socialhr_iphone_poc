//
//  EmployeeTimelineDataController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 9/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmployeeTimelineDataController : NSObject

+(NSMutableArray*)getEmployeeTimeTimelineActivites:(NSInteger)employeeId;

@end
