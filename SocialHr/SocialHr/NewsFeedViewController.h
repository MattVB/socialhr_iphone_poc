//
//  NewsFeedViewController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface NewsFeedViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout, TTTAttributedLabelDelegate>

@property NSArray *activities;
- (IBAction)menu:(id)sender;

@end
