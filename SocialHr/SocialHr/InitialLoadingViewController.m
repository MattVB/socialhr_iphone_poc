//
//  InitialLoadingViewController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 16/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "InitialLoadingViewController.h"
#import "UserDetailsDataController.h"
#import "LoadingNotificationHelper.h"


@interface InitialLoadingViewController ()

@end

@implementation InitialLoadingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Show the hud and do the auto sign in.
    [LoadingNotificationHelper showHudAndStartAction:self.view selector:@selector(doAutoSignIn) toTarget:self withObject:nil];
}


- (void)doAutoSignIn
{
    // Check for if the user should be logged in automatically
    if ([UserDetailsDataController doAppStartAutoAuthentication])
    {
        // Redirect straight through
        [self performSegueWithIdentifier:@"autoLoginSuccess" sender:nil];
        return;
    }
    
    // Redirect to the login view
    [self performSegueWithIdentifier:@"login" sender:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
