//
//  NewsFeedActivityCell.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsFeedActivityCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelEmployeeName;
@property (weak, nonatomic) IBOutlet UILabel *labelShortDescription;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;
@property (weak, nonatomic) IBOutlet UIImageView *imageEmployee;
@property (weak, nonatomic) IBOutlet UIImageView *imageActivityType;
@property (weak, nonatomic) IBOutlet UILabel *labelAddedBy;
@property (weak, nonatomic) IBOutlet UILabel *labelLikesComments;

@property (weak, nonatomic) IBOutlet UIImageView *imageDocument;

@end
