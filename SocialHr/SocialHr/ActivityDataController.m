//
//  ActivityDataController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 7/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "ActivityDataController.h"
#import "WebServiceClientHelper.h"

@implementation ActivityDataController

+(void)likeActivity:(NSInteger)activityId
{
    // Create and populate the parameter dictionary
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[NSNumber numberWithInteger:activityId] forKey:@"employeeActivityId"];
    
    // Call the webservice
    [WebServiceClientHelper makeWebServiceCallAndReturnResult:@"/Mobile/LikeEmployeeActivity" parameters: parameters];

}

+(void)commentOnActivity:(NSInteger)activityId comment:(NSString*)comment
{
    // Create and populate the parameter dictionary
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[NSNumber numberWithInteger:activityId] forKey:@"employeeActivityId"];
    [parameters setValue:comment forKey:@"comment"];
    
    // Call the webservice
    [WebServiceClientHelper makeWebServiceCallAndReturnResult:@"/Mobile/CommentOnEmployeeActivity" parameters: parameters];
}

+(Activity*)getActivity:(NSInteger)activityId
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[NSNumber numberWithInteger:activityId] forKey:@"employeeActivityId"];
    
    // Call the webservice
    NSDictionary *resultDictionary = [WebServiceClientHelper makeWebServiceCallAndReturnResult:@"/Mobile/GetEmployeeActivity" parameters: parameters];
    
    Activity *result = [[Activity alloc]initFromDictionary:resultDictionary];
    
    return result;
}


@end
