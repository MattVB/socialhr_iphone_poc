//
//  EmployeeSearchViewController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "EmployeeSearchViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "EmployeeDetails.h"
#import "EmployeeSearchDataController.h"
#import "EmployeeTimelineViewedEmployeeDataController.h"

@interface EmployeeSearchViewController ()

@end

@implementation EmployeeSearchViewController

@synthesize filteredEmployees;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Put the focus into the field
    [self.searchBar becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    [self.slidingViewController setAnchorRightRevealAmount:280.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)menu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.filteredEmployees.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmployeeCell"];
    
    EmployeeDetails *employeeDetails = [self.filteredEmployees objectAtIndex:indexPath.row];
    cell.textLabel.text = employeeDetails.employeeName;
    
    return cell;
}

-(void)refreshItemsList:(NSString*)searchText
{
    // Filter the list
    self.filteredEmployees = [EmployeeSearchDataController doEmployeeSearch:searchText];
    
    // Refresh the table
    [self.tableView reloadData];
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refreshItemsList:searchText];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Set the employee to be viewed
    EmployeeDetails *employeeDetails = [self.filteredEmployees objectAtIndex:indexPath.row];
    [EmployeeTimelineViewedEmployeeDataController setEmployeeId:employeeDetails.id];
    
    // Push to the view
    [self performSegueWithIdentifier:@"showEmployee" sender:nil];
}

@end
