//
//  ActivityComment.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 7/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "ActivityComment.h"
#import "DictionaryToModelHelper.h"

@implementation ActivityComment

@synthesize  comment = _comment;
@synthesize commenterEmployeeId = _commenterEmployeeId;
@synthesize  commenterEmployeeName = _commenterEmployeeName;
@synthesize commenterPhotoUrl = _commenterPhotoUrl;

-(id)initFromDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    
    if (self)
    {
        self.commenterEmployeeName = [DictionaryToModelHelper getStringFromDictionary:dictionary withKey:@"CommenterEmployeeName"];
        self.commenterEmployeeId = [DictionaryToModelHelper getIntegerFromDictionary:dictionary withKey:@"CommenterEmployeeId"];
        self.comment = [DictionaryToModelHelper getStringFromDictionary:dictionary withKey:@"Comment"];
        self.commenterPhotoUrl = [DictionaryToModelHelper getStringFromDictionary:dictionary withKey:@"CommenterPhotoUrl"];
        
        
    }
    
    return self;
}

@end
