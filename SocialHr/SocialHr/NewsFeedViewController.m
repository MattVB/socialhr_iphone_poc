//
//  NewsFeedViewController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "NewsFeedViewController.h"
#import "NewsFeedDataController.h"
#import "Activity.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "ActivityDetailViewController.h"
#import "NewsFeedActivityCell.h"
#import "ImageViewHelper.h"
#import "WebServiceUrlHelper.h"
#import "LoadingNotificationHelper.h"
#import "UIImageView+UIImageView_SourceUrl.h"
#import "EmployeeTimelineViewedEmployeeDataController.h"
#import "EmployeeLinkHelper.h"


@interface NewsFeedViewController ()

@end

@implementation NewsFeedViewController

@synthesize activities = _activities;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Load the data
    self.activities = [NewsFeedDataController getNewsFeedData];
    
    // Post notification that the loading is complete
    [LoadingNotificationHelper postLoadingCompleteNotification:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    [self.slidingViewController setAnchorRightRevealAmount:280.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] objectAtIndex:0];
        ActivityDetailViewController *viewController = [segue destinationViewController];
        viewController.activity = [self.activities objectAtIndex:indexPath.row];
    }
}

- (IBAction)menu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.activities.count;
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    // Call the helper to handle the request
    [EmployeeLinkHelper handleLinkToEmployeePressed:url fromViewController:self];
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NewsFeedActivityCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ActivityCell" forIndexPath:indexPath];
    
    Activity *activity = [self.activities objectAtIndex:indexPath.row];
    
    // Populate the labels
    cell.labelShortDescription.text = activity.shortDescription;
    cell.labelDate.text = activity.formattedDate;
    
    // Add in the employee name label as a link
    [EmployeeLinkHelper populateLabel:cell.labelEmployeeName withLinkForEmployeeWithId:activity.employeeId andEmployeeName:activity.employeeName withDelegate:self];
    
    // Include the added by if required
    if ([activity.employeeName isEqualToString:activity.addedByEmployeeName])
    {
        cell.labelAddedBy.text = @"";
    }
    else
    {
        // Add in the "added by" employee name as a link label
        [EmployeeLinkHelper populateLabel:cell.labelAddedBy withLinkForEmployeeWithId:activity.addedByEmployeeId andEmployeeName:activity.addedByEmployeeName withDelegate:self withSuffix:@":"];
    }
    
    
    // Clear the images
    cell.imageActivityType.image =  nil;
    cell.imageDocument.image = nil;
    cell.imageEmployee.image = nil;
    
    // Populate the images
    [ImageViewHelper downloadImageInImageView:cell.imageActivityType withUrlString:[WebServiceUrlHelper createFullUrlForResource:activity.employeeActivityTypeIconUrl]];
    [ImageViewHelper downloadImageInImageView:cell.imageEmployee withUrlString:[WebServiceUrlHelper createFullUrlForResource:activity.employeePhotoUrl]];
    
    // Add on the image document if applicable
    if (activity.imageDocumentUrl)
    {
        [ImageViewHelper downloadImageInImageView:cell.imageDocument withUrlString:[WebServiceUrlHelper createFullUrlForResource:activity.imageDocumentUrl]];
        cell.labelContent.text = @"";
    }
    else
    {
        // Clear the image
        cell.imageDocument.image = nil;
        [cell.imageDocument setImageUrl:nil];
        
        // Set the cell text and resize
        // Set the lable and resize
        CGSize maximumLabelSize = CGSizeMake(220,82);
        CGSize expectedLabelSize = [activity.content sizeWithFont:[UIFont systemFontOfSize:14]
                                                constrainedToSize:maximumLabelSize];
        
        cell.labelContent.text = activity.content;
        cell.labelContent.frame = CGRectMake(cell.labelContent.frame.origin.x, cell.labelContent.frame.origin.y, expectedLabelSize.width, expectedLabelSize.height);
        
    }
    
    cell.labelLikesComments.text = [activity formatLikeCommentString];
    
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the activity
    Activity *activity = [self.activities objectAtIndex:indexPath.row];
    
    CGFloat width = 295;
    
    // Check for if there is an image - if so give full height
    if (activity.imageDocumentUrl)
    {
        return CGSizeMake(width, 244);
    }
    
    CGSize maximumLabelSize = CGSizeMake(220,82);
    CGSize expectedLabelSize = [activity.content sizeWithFont:[UIFont systemFontOfSize:14]
                                           constrainedToSize:maximumLabelSize];

    return CGSizeMake(width, 165 + expectedLabelSize.height);
}

@end
