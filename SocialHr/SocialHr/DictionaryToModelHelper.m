//
//  DictionaryToModelHelper.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 7/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "DictionaryToModelHelper.h"

@implementation DictionaryToModelHelper

+(NSString*)getStringFromDictionary:(NSDictionary*)dictionary withKey:(NSString*)key
{
    // Get the object
    NSObject *result = [dictionary objectForKey:key];
    
    // Check for if the object is null
    if (result == [NSNull null])
    {
        return nil;
    }
    
    return (NSString*)result;
}

+(NSInteger)getIntegerFromDictionary:(NSDictionary*)dictionary withKey:(NSString*)key
{
    NSNumber *number = [dictionary objectForKey:key];
    return [number integerValue];
}

+(BOOL)getBoolFromDictionary:(NSDictionary*)dictionary withKey:(NSString*)key
{
    NSNumber *number = [dictionary objectForKey:key];
    return [number boolValue];
}

@end
