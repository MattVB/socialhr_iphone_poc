//
//  LoginViewController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 9/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "LoginViewController.h"
#import "UserDetailsDataController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    // Populate the login screen
    self.textWebSiteUrl.text = [UserDetailsDataController getBaseUrl];
    self.textUserName.text = [UserDetailsDataController getUserName];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    // Check for if the user should be logged in automatically
    if ([UserDetailsDataController doAppStartAutoAuthentication])
    {
        // Redirect straight through
        [self performSegueWithIdentifier:@"loginSuccess" sender:nil];
        return;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender
{
    // Check for any text fields empty
    if (self.textWebSiteUrl.text.length == 0 || self.textUserName.text.length == 0 || self.textPassword.text.length == 0)
    {
        // Show UI Alert view
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Login Failed" message:@"You must complete all fields to be able to login." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    // Attempt to login
    BOOL result = [UserDetailsDataController authenticateUser:self.textUserName.text password:self.textPassword.text baseUrl:self.textWebSiteUrl.text];
    
    if (!result)
    {
        // Show UI Alert view
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Login Failed" message:@"Please check details and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return;
    }

    // Got to view
    [self performSegueWithIdentifier:@"loginSuccess" sender:nil];
}
@end
