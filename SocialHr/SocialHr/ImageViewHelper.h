//
//  ImageViewHelper.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageViewHelper : NSObject

+(void)downloadImageInImageView:(UIImageView *)imageView withURL:(NSURL *)imageUrl;

+(void)downloadImageInImageView:(UIImageView *)imageView withUrlString:(NSString *)imageUrl;

@end
