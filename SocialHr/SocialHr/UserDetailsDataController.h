//
//  UserDetailsDataController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDetailsDataController : NSObject

+(NSString*)getBaseUrl;

+(NSString*)getUserName;

+(NSString*)getPassword;

+(BOOL)authenticateUser:(NSString*)userName password:(NSString*) password baseUrl:(NSString*)baseUrl;

+(BOOL)doAppStartAutoAuthentication;

+(void)logout;

@end
