//
//  NewsFeedActivity.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Activity : NSObject

    @property NSString *shortDescription;
    @property NSString *content;
    @property NSString *employeeName;
    @property NSString *addedByEmployeeName;
    @property NSString *imageDocumentUrl;
    @property NSString *employeePhotoUrl;
    @property NSString *employeeActivityTypeIconUrl;
    @property NSString *formattedDate;
    @property NSInteger likeCount;
    @property NSInteger commentCount;
    @property NSInteger id;

@property NSInteger employeeId;
@property NSInteger addedByEmployeeId;

    @property BOOL isLikeable;
    @property BOOL isCommentable;

    @property NSMutableArray *likes;
    @property NSMutableArray *comments;

    -(id)initFromDictionary:(NSDictionary*)dictionary;

    -(NSString*)formatLikeCommentString;

@end
