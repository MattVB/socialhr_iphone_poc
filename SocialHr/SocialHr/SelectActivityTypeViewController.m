//
//  SelectActivityTypeViewController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "SelectActivityTypeViewController.h"

@interface SelectActivityTypeViewController ()

@end

@implementation SelectActivityTypeViewController

@synthesize availableTypes;
@synthesize  delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
return self.availableTypes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TypeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = [self.availableTypes objectAtIndex:indexPath.row];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // Get the selected value
    NSString *selectedValue = [self.availableTypes objectAtIndex:indexPath.row];
    
    // Call the delegate
    [self.delegate selectActivityTypeControllerDidFinish:self selectedType:selectedValue];
}

@end
