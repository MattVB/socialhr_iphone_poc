//
//  ImageViewHelper.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "ImageViewHelper.h"
#import "UIImageView+UIImageView_SourceUrl.h"

@implementation ImageViewHelper

static NSCache *imageCache;

+(void)downloadImageInImageView:(UIImageView *)imageView withUrlString:(NSString *)imageUrl
{
    NSURL *url = [NSURL URLWithString:imageUrl];
    [ImageViewHelper downloadImageInImageView:imageView withURL:url];
}

+(void)downloadImageInImageView:(UIImageView *)imageView withURL:(NSURL *)imageUrl {
    if(!imageView || !imageUrl)
        return;
    
    // Set the URL
    [imageView setImageUrl:imageUrl];
    
    // Check if the image is in the cache
    UIImage *imageFromCache = [self tryGetImageFromCache:imageUrl];
    
    // If found from cache then use image from cache.
    if (imageFromCache)
    {
        [imageView setImage:imageFromCache];
        return;
    }
    
    // Not in cache - then download in seperate thread.
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          imageView,@"imageView",
                          imageUrl,@"imageUrl", nil];
    [NSThread detachNewThreadSelector:@selector(downloadImage:) toTarget:self withObject:dict];
}

+(void)downloadImage:(NSDictionary *)dict {
    UIImageView *imageView = [dict valueForKey:@"imageView"];
    NSURL *imageUrl = [dict valueForKey:@"imageUrl"];
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
    
    // Do check in here
    NSURL *currentImageUrl = [imageView getImageUrl];
    
    // Only populate the image if the URL is still the same
    if ([[currentImageUrl absoluteString] isEqualToString:[imageUrl absoluteString]])
         {
             [imageView setImage:image];
         }
    
    if (image)
    {
        // Add the item to the cache
        [self addImageToCache:image forImageUrl:imageUrl];
    }
    
}

+(UIImage*)tryGetImageFromCache:(NSURL*)imageUrl
{
    if (!imageCache)
    {
        return nil;
    }
    
    return [imageCache objectForKey:imageUrl];
}

+(void)addImageToCache:(UIImage*)image forImageUrl:(NSURL*)imageUrl
{
    // If cache not created then create the cache
    if (!imageCache)
    {
        imageCache = [[NSCache alloc]init];
    }
    
    // Add the item to the cache
    [imageCache setObject:image forKey:imageUrl];
}

@end
