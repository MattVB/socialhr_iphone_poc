//
//  ActivityDetailCell.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityDetailCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelEmployeeName;
@property (weak, nonatomic) IBOutlet UILabel *labelShortDescription;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;
@property (weak, nonatomic) IBOutlet UIImageView *imageEmployee;
@property (weak, nonatomic) IBOutlet UIImageView *imageActivityType;
@property (weak, nonatomic) IBOutlet UILabel *labelAddedBy;

@end
