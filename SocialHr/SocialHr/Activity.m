//
//  NewsFeedActivity.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "Activity.h"
#import "DictionaryToModelHelper.h"
#import "ActivityLike.h"
#import "ActivityComment.h"

@implementation Activity

@synthesize shortDescription = _shortDescription;
@synthesize content = _content;
@synthesize employeeName = _employeeName;
@synthesize addedByEmployeeName = _addedByEmployeeName;
@synthesize imageDocumentUrl = _imageDocumentUrl;
@synthesize employeePhotoUrl = _employeePhotoUrl;
@synthesize employeeActivityTypeIconUrl = _employeeActivityTypeIconUrl;
@synthesize formattedDate = _formattedDate;
@synthesize likeCount = _likeCount;
@synthesize commentCount = _commentCount;
@synthesize isLikeable = _isLikeable;
@synthesize isCommentable = _isCommentable;
@synthesize  likes = _likes;
@synthesize comments = _comments;
@synthesize  employeeId = _employeeId;
@synthesize  addedByEmployeeId = _addedByEmployeeId;

-(id)initFromDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.shortDescription = [dictionary objectForKey:@"ShortDescription"];
        self.content = [DictionaryToModelHelper getStringFromDictionary:dictionary withKey:@"Content"];
        self.employeeName = [dictionary objectForKey:@"EmployeeName"];
        self.addedByEmployeeName = [dictionary objectForKey:@"AddedByEmployeeName"];
        self.imageDocumentUrl = [DictionaryToModelHelper getStringFromDictionary:dictionary withKey:@"ImageDocumentUrl"];
        self.employeePhotoUrl = [dictionary objectForKey:@"EmployeePhotoUrl"];
        self.employeeActivityTypeIconUrl = [dictionary objectForKey:@"EmployeeActivityTypeIconUrl"];
        self.formattedDate = [dictionary objectForKey:@"FormattedDate"];
        
        self.likeCount = [DictionaryToModelHelper getIntegerFromDictionary:dictionary withKey:@"LikeCount"];
        self.commentCount = [DictionaryToModelHelper getIntegerFromDictionary:dictionary withKey:@"CommentCount"];
        self.id = [DictionaryToModelHelper getIntegerFromDictionary:dictionary withKey:@"Id"];
        self.employeeId = [DictionaryToModelHelper getIntegerFromDictionary:dictionary withKey:@"EmployeeId"];
        self.addedByEmployeeId = [DictionaryToModelHelper getIntegerFromDictionary:dictionary withKey:@"AddedByEmployeeId"];
        
        self.isLikeable = [DictionaryToModelHelper getBoolFromDictionary:dictionary withKey:@"IsLikeable"];
        self.isCommentable = [DictionaryToModelHelper getBoolFromDictionary:dictionary withKey:@"IsCommentable"];
        
        self.likes = [[NSMutableArray alloc]init];
        self.comments = [[NSMutableArray alloc]init];
        
        NSArray *likeItems = [dictionary objectForKey:@"Likes"];
        
        for (NSDictionary *likeItemDictionary in likeItems) {
            ActivityLike *like = [[ActivityLike alloc]initFromDictionary:likeItemDictionary];
            [self.likes addObject:like];
        }
        
        NSArray *commentItems = [dictionary objectForKey:@"Comments"];
        
        for (NSDictionary *commentItemDictionary in commentItems) {
            ActivityComment *comment = [[ActivityComment alloc]initFromDictionary:commentItemDictionary];
            [self.comments addObject:comment];
        }
    }
    
    return self;
}

   -(NSString*)formatLikeCommentString
    {
        NSString *result = @"";
        NSNumber *likeCountNumber = [NSNumber numberWithInteger:self.likeCount];
        NSNumber *commentCountNumber = [NSNumber numberWithInteger:self.commentCount];
        
        if (self.likeCount > 0)
        {
            result = [@"Likes: " stringByAppendingString:[likeCountNumber stringValue]];
            
            if (self.commentCount > 0)
            {
                result = [result stringByAppendingString: @"  "];
            }
        }
        
        if (self.commentCount > 0)
        {
            NSString *commentString = [@"Comments: " stringByAppendingString:[commentCountNumber stringValue]];
            result= [result stringByAppendingString:commentString ];
        }
        
        return result;
    }


@end
