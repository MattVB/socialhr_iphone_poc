//
//  WebServiceClientHelper.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "WebServiceClientHelper.h"
#import "WebServiceUrlHelper.h"
#import "UserDetailsDataController.h"

@implementation WebServiceClientHelper

+(NSDictionary*)makeWebServiceCallAndReturnResult:(NSString *)urlSuffix parameters:(NSMutableDictionary *)parameters
{
    // Add in the userName and password into the parameter dictionary. These should be passed through on all calls.
    [parameters setObject:[UserDetailsDataController getUserName] forKey:@"userName"];
    [parameters setObject:[UserDetailsDataController getPassword] forKey:@"password"];
    
    
    // Convert the dictionary to json data
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters
                                                       options:kNilOptions
                                                         error:&error];
    
    // Create the URl
    NSURL *url = [NSURL URLWithString:[WebServiceUrlHelper createFullUrl:urlSuffix]];
    
    // Prepare the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"json" forHTTPHeaderField:@"Data-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]]  forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:jsonData];
    
    // Call the method
    NSError *errorReturned = nil;
    NSURLResponse *theResponse =[[NSURLResponse alloc]init];
    NSData *result = [NSURLConnection sendSynchronousRequest:request
                                           returningResponse:&theResponse
                                                       error:&errorReturned];
    
    // Deserialize the result and return
    NSError *serializationError = nil;
    NSDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingMutableContainers error:&serializationError];
    
    return resultDictionary;
}

@end
