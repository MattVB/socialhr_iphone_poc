//
//  SelectActivityTypeViewController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectActivityTypeViewControllerDelegate;


@interface SelectActivityTypeViewController : UITableViewController

@property NSArray *availableTypes;

@property (weak, nonatomic) id <SelectActivityTypeViewControllerDelegate> delegate;

@end


@protocol SelectActivityTypeViewControllerDelegate <NSObject>
- (void)selectActivityTypeControllerDidFinish:(SelectActivityTypeViewController *)controller selectedType:(NSString *)selectedType;
@end