//
//  EmployeeDetails.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 9/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "EmployeeDetails.h"
#import "DictionaryToModelHelper.h"

@implementation EmployeeDetails

@synthesize id;
@synthesize employeeName;
@synthesize employeePhotoUrl;
@synthesize position;
@synthesize department;

-(id)initFromDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        self.employeeName = [DictionaryToModelHelper getStringFromDictionary:dictionary withKey:@"EmployeeName"];
        self.employeePhotoUrl = [DictionaryToModelHelper getStringFromDictionary:dictionary withKey:@"EmployeePhotoUrl"];
        self.department = [DictionaryToModelHelper getStringFromDictionary:dictionary withKey:@"Department"];
        self.position = [DictionaryToModelHelper getStringFromDictionary:dictionary withKey:@"Position"];
        self.id = [DictionaryToModelHelper getIntegerFromDictionary:dictionary withKey:@"Id"];
    }
    
    return self;
}

@end
