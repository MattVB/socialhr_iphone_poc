//
//  EmployeeTimelineViewController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 9/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "EmployeeTimelineViewController.h"
#import "MenuViewController.h"
#import "EmployeeDetailsDataController.h"
#import "TimelineHeaderView.h"
#import "ImageViewHelper.h"
#import "WebServiceUrlHelper.h"
#import "EmployeeTimelineDataController.h"
#import "TimelineActivityCell.h"
#import "Activity.h"
#import "ActivityDetailViewController.h"
#import "EmployeeTimelineViewedEmployeeDataController.h"
#import "LoadingNotificationHelper.h"
#import "UIImageView+UIImageView_SourceUrl.h"
#import "EmployeeLinkHelper.h"


@interface EmployeeTimelineViewController ()

@end

@implementation EmployeeTimelineViewController

@synthesize employeeId;
@synthesize  employeeDetails;
@synthesize  activities;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Get the employee details
    self.employeeId = [EmployeeTimelineViewedEmployeeDataController getEmployeeId];
    self.employeeDetails = [EmployeeDetailsDataController getEmployeeDetails:self.employeeId];
    
    // Get the activities
    self.activities = [EmployeeTimelineDataController getEmployeeTimeTimelineActivites:self.employeeDetails.id];
    
    // Set the navigation title
    self.navigationItem.title = self.employeeDetails.employeeName;
    
    NSInteger stackCount = self.navigationController.viewControllers.count;
    
    if (stackCount == 1)
    {
        UIBarButtonItem *menuButton = [[UIBarButtonItem alloc]initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(menu:)];
        self.navigationItem.leftBarButtonItem = menuButton;
    }
    
    // Post notification that the loading is complete
    [LoadingNotificationHelper postLoadingCompleteNotification:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    [self.slidingViewController setAnchorRightRevealAmount:280.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)menu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    // Call the helper to handle the request
    [EmployeeLinkHelper handleLinkToEmployeePressed:url fromViewController:self];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.activities.count;
}

-(UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    TimelineHeaderView *headerCell = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"TimelineHeader" forIndexPath:indexPath];
    
    headerCell.labelEmployeeName.text = self.employeeDetails.employeeName;
    headerCell.labelDepartment.text = [@"Department: " stringByAppendingString:self.employeeDetails.department];
    headerCell.labelPosition.text = [@"Position: " stringByAppendingString:self.employeeDetails.position];
    [ImageViewHelper downloadImageInImageView:headerCell.imagePhoto withUrlString:[WebServiceUrlHelper createFullUrlForResource:self.employeeDetails.employeePhotoUrl]];
    
    return headerCell;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TimelineActivityCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ActivityCell" forIndexPath:indexPath];
    
    Activity *activity = [self.activities objectAtIndex:indexPath.row];
    
    // Populate the labels
    cell.labelShortDescription.text = activity.shortDescription;
    cell.labelDate.text = activity.formattedDate;

    
    // Include the added by if required
    if ([activity.employeeName isEqualToString:activity.addedByEmployeeName])
    {
        cell.labelAddedBy.text = @"";
    }
    else
    {
        // Add in the "added by" employee name as a link label
        [EmployeeLinkHelper populateLabel:cell.labelAddedBy withLinkForEmployeeWithId:activity.addedByEmployeeId andEmployeeName:activity.addedByEmployeeName withDelegate:self withSuffix:@":"];
    }
    
    
    // Clear the images
    cell.imageActivityType.image =  nil;
    cell.imageDocument.image = nil;
    
    // Populate the images
    [ImageViewHelper downloadImageInImageView:cell.imageActivityType withUrlString:[WebServiceUrlHelper createFullUrlForResource:activity.employeeActivityTypeIconUrl]];
    
    // Add on the image document if applicable
    if (activity.imageDocumentUrl)
    {
        [ImageViewHelper downloadImageInImageView:cell.imageDocument withUrlString:[WebServiceUrlHelper createFullUrlForResource:activity.imageDocumentUrl]];
            cell.labelContent.text = @"";
    }
    else
    {
        // Clear the image
        cell.imageDocument.image = nil;
        [cell.imageDocument setImageUrl:nil];
        
        // Set the lable and resize
        CGSize maximumLabelSize = CGSizeMake(220,82);
        CGSize expectedLabelSize = [activity.content sizeWithFont:[UIFont systemFontOfSize:14]
                                                constrainedToSize:maximumLabelSize];
        
        cell.labelContent.text = activity.content;
        cell.labelContent.frame = CGRectMake(cell.labelContent.frame.origin.x, cell.labelContent.frame.origin.y, expectedLabelSize.width, expectedLabelSize.height);
    }
    
    cell.labelLikesComments.text = [activity formatLikeCommentString];
    
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the activity
    Activity *activity = [self.activities objectAtIndex:indexPath.row];
    
    CGFloat width = 295;
    
    // Check for if there is an image - if so give full height
    if (activity.imageDocumentUrl)
    {
        return CGSizeMake(width, 201);
    }
    
    CGSize maximumLabelSize = CGSizeMake(220,82);
    CGSize expectedLabelSize = [activity.content sizeWithFont:[UIFont systemFontOfSize:14]
                                            constrainedToSize:maximumLabelSize];
    
    return CGSizeMake(width, 122 + expectedLabelSize.height);
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] objectAtIndex:0];
        ActivityDetailViewController *viewController = [segue destinationViewController];
        viewController.activity = [self.activities objectAtIndex:indexPath.row];
    }
    
    if ([[segue identifier] isEqualToString:@"addActivity"]) {
        AddActivityViewController *viewController = [segue destinationViewController];
        viewController.employeeId = self.employeeId;
        viewController.delegate = self;
    }
}

-(void)addActivityComplete:(AddActivityViewController *)controller
{
    // Reload the view
    // Get the activities
    self.activities = [EmployeeTimelineDataController getEmployeeTimeTimelineActivites:self.employeeDetails.id];
    [self.collectionView reloadData];
    
    // Dismiss the controller
    [controller.navigationController popViewControllerAnimated:YES];
}

@end
