//
//  DictionaryToModelHelper.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 7/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DictionaryToModelHelper : NSObject

+(NSString*)getStringFromDictionary:(NSDictionary*)dictionary withKey:(NSString*)key;

+(NSInteger)getIntegerFromDictionary:(NSDictionary*)dictionary withKey:(NSString*)key;

+(BOOL)getBoolFromDictionary:(NSDictionary*)dictionary withKey:(NSString*)key;

@end
