//
//  UIImageView+UIImageView_SourceUrl.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 17/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "UIImageView+UIImageView_SourceUrl.h"
#import <objc/runtime.h>

static char const * const ImageUrlTagKey = "ImageUrl";

@implementation UIImageView (UIImageView_SourceUrl)


-(void)setImageUrl:(NSURL*)imageUrl
{
    objc_setAssociatedObject(self, ImageUrlTagKey, imageUrl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSURL*)getImageUrl
{
     return objc_getAssociatedObject(self, ImageUrlTagKey);
}

@end
