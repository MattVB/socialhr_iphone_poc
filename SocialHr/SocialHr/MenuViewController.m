 //
//  MenuViewController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 9/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "MenuViewController.h"
#import "EmployeeDetailsDataController.h"
#import "UserDetailsDataController.h"
#import "EmployeeTimelineViewController.h"
#import "EmployeeTimelineViewedEmployeeDataController.h"
#import "ImageViewHelper.h"
#import "WebServiceUrlHelper.h"
#import "MBProgressHUD.h"
#import "LoadingNotificationHelper.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

@synthesize employeeDetails;
@synthesize employees;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Load the employee details
    self.employeeDetails = [EmployeeDetailsDataController getEmployeeDetails:0];
    // Get the employees
    self.employees = [EmployeeDetailsDataController getEmployeesForUser];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideLoading) name:[LoadingNotificationHelper getNotificationName] object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hideLoading
{
    // Hide the HUD
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (employees.count > 0)
    {
        return 3;
    }
    
    return 2;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"Favourites";
    }
    
    if (section == 1 && employees.count > 0)
    {
        return @"Employees";
    }
    
    return @"Account";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 3;
    }
    
    if (section == 1 && employees.count > 0)
    {
        return self.employees.count;
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            cell.textLabel.text = @"News Feed";
        }
        
        if (indexPath.row == 1)
        {
            cell.textLabel.text = self.employeeDetails.employeeName;
        }
        
        if (indexPath.row == 2)
        {
            cell.textLabel.text = @"Employee Search";
        }
        
        return cell;
    }
    
    if (indexPath.section == 1 && employees.count > 0)
    {
        EmployeeDetails *selectedEmployeeDetails = [self.employees objectAtIndex:indexPath.row];
        cell.textLabel.text = selectedEmployeeDetails.employeeName;
        
        return cell;
    }
    
    
    cell.textLabel.text = @"Logout";
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
            {
                // Go to the newsfeed
                [LoadingNotificationHelper showHudAndStartAction:self.view selector:@selector(goToNewsFeed) toTarget:self withObject:nil];
                
                return;
            }
        
        if (indexPath.row == 1)
        {
            // Go to the timeline
            [LoadingNotificationHelper showHudAndStartAction:self.view selector:@selector(goToTimelineForEmployee:) toTarget:self withObject:self.employeeDetails];
            
            return;
        }
        
        if (indexPath.row == 2)
        {
            UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EmployeeSearchNavigationController"];
            [self displayViewContoller:newTopViewController];
        }
        
        return;
    }
    
    if (indexPath.section == 1 && employees.count > 0)
    {
        // Get the employee
        EmployeeDetails *selectedEmployeeDetails = [self.employees objectAtIndex:indexPath.row];
        
        // Go to the timeline
        [LoadingNotificationHelper showHudAndStartAction:self.view selector:@selector(goToTimelineForEmployee:) toTarget:self withObject:selectedEmployeeDetails];

        return;
    }
    
    // Logout
    [UserDetailsDataController logout];
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Login"];
    [self displayViewContoller:newTopViewController];
}

-(void)goToTimelineForEmployee:(EmployeeDetails*)employee
{
    [EmployeeTimelineViewedEmployeeDataController setEmployeeId:employee.id];
    
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TimelineNavigationController"];
    
    [self displayViewContoller:newTopViewController];
}

-(void)goToNewsFeed
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsFeedNavigationController"];
    [self displayViewContoller:newTopViewController];
}

-(void)displayViewContoller:(UIViewController*)newTopViewContoller
{
    [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewContoller;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }];
}

@end
