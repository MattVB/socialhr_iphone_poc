//
//  AddActivityViewController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "AddActivityViewController.h"
#import "AddActivityDataController.h"
#import "ImageViewHelper.h"

@interface AddActivityViewController ()

@end

@implementation AddActivityViewController

@synthesize employeeId;
@synthesize availableActivityTypes;
@synthesize  delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Get the list of available types and choose the first one as the default
    self.availableActivityTypes = [AddActivityDataController getAvailableActivityTypes:self.employeeId];
    self.labelActivityType.text = [self.availableActivityTypes objectAtIndex:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"selectType"])
    {
        SelectActivityTypeViewController *selectActivityTypeController = [segue destinationViewController];
        selectActivityTypeController.delegate = self;
        selectActivityTypeController.availableTypes = self.availableActivityTypes;
    }
}

-(void)selectActivityTypeControllerDidFinish:(SelectActivityTypeViewController *)controller selectedType:(NSString *)selectedType
{
    // Update the label
    self.labelActivityType.text = selectedType;
    
    // Dismiss the controller
    [controller.navigationController popViewControllerAnimated:YES];
}

- (IBAction)done:(id)sender
{
    NSData *imageData = nil;
    
    if (self.imagePhoto.image)
    {
        imageData = UIImageJPEGRepresentation(self.imagePhoto.image, 0.25);
    }
    
    // Add the activity
    [AddActivityDataController addActivity:self.employeeId activityType:self.labelActivityType.text shortDescription:self.textShortDescription.text content:self.textViewContent.text imageData:imageData];
    
    // Call the delegate
    [self.delegate addActivityComplete:self];
}

- (IBAction)buttonAddPhoto:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Add Photo" message:@"Take new photo or use existing?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Take new photo", @"Use existing photo", nil];
    [alertView show];
   }


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        // Cancel
        return;
    }
    
     UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    
    if (buttonIndex == 1)
    {
        // Take new
        cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    
    if (buttonIndex == 2)
    {
        // Select existing
        cameraUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    
    cameraUI.delegate = self;    
    [self presentViewController:cameraUI animated:YES completion:nil];

}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *editedImage = (UIImage *) [info objectForKey:
                               UIImagePickerControllerOriginalImage];
    
    self.imagePhoto.image =  editedImage;
    
    [picker dismissViewControllerAnimated:YES completion:nil];

}

@end
