//
//  ShowImageDocumentViewController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowImageDocumentViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *image;

@property NSString *imageUrl;

@end
