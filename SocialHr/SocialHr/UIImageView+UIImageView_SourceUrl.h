//
//  UIImageView+UIImageView_SourceUrl.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 17/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImageView (UIImageView_SourceUrl)

-(void)setImageUrl:(NSURL*)imageUrl;

-(NSURL*)getImageUrl;

@end
