//
//  EmployeeSearchDataController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "EmployeeSearchDataController.h"
#import "EmployeeDetails.h"
#import "WebServiceClientHelper.h"

@implementation EmployeeSearchDataController

+(NSArray*)doEmployeeSearch:(NSString *)searchText
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:searchText forKey:@"searchText"];
    
    // Call the webservice
    NSDictionary *resultDictionary = [WebServiceClientHelper makeWebServiceCallAndReturnResult:
                                      @"/Mobile/DoEmployeeSearch" parameters: parameters];
    
    // Get the activities
    NSArray *employees = [resultDictionary objectForKey:@"Employees"];
    
    // Create an object for each item and then return
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    for (NSDictionary *employeeDictionary in employees)
    {
        [result addObject:[[EmployeeDetails alloc]initFromDictionary:employeeDictionary]];
    }
    
    return result;
}

@end
