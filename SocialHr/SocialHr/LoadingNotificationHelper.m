//
//  LoadingNotificationHelper.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 17/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "LoadingNotificationHelper.h"
#import "MBProgressHUD.h"

@implementation LoadingNotificationHelper

NSString *const NotificationName    = @"SocialHrLoadingComplete";

+(void)postLoadingCompleteNotification:(UIViewController *)fromViewController
{
    // Post the notification
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName object:fromViewController];
}

+(NSString*)getNotificationName
{
    return NotificationName;
}

+(void)showHudAndStartAction:(UIView *)view selector:(SEL)selector toTarget:(id)target withObject:(id)argument
{
    // Create the HUD
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
    
    // Add to the view
	[view addSubview:hud];
    
    // Show the hud
    [hud show:YES];
    
    // Start the action on a seperate thread
    [NSThread detachNewThreadSelector:selector toTarget:target withObject:argument];
}

@end
