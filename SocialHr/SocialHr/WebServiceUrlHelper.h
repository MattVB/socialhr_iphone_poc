//
//  WebServiceUrlHelper.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServiceUrlHelper : NSObject

+(NSString*)createFullUrl:(NSString*)urlSuffix;

+(NSString*)createFullUrlForResource:(NSString*)urlSuffix;

@end
