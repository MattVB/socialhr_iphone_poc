//
//  WebServiceUrlHelper.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "WebServiceUrlHelper.h"
#import "UserDetailsDataController.h"

@implementation WebServiceUrlHelper


+(NSString*)createFullUrl:(NSString*)urlSuffix
{
    // Get the url prefix from the user data
    NSString *urlPrefix = [UserDetailsDataController getBaseUrl];
    
    return [urlPrefix stringByAppendingString:urlSuffix];
}

+(NSString*)createFullUrlForResource:(NSString *)urlSuffix
{
    NSString *urlPrefix = [UserDetailsDataController getBaseUrl];
    
    NSRange firstSlash = [urlPrefix rangeOfString:@"/"];
    firstSlash.location = firstSlash.location + 2;
    firstSlash.length = urlPrefix.length - firstSlash.location;
    NSRange nextSlash = [urlPrefix rangeOfString:@"/" options:NSCaseInsensitiveSearch range:firstSlash];
    
    NSString *truncatedPrefix = [urlPrefix substringToIndex:nextSlash.location];
    
    
    return [truncatedPrefix stringByAppendingString:urlSuffix];
}

@end
