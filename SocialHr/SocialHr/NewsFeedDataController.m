//
//  NewsFeedDataController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "NewsFeedDataController.h"
#import "WebServiceClientHelper.h"
#import "Activity.h"

@implementation NewsFeedDataController

+(NSMutableArray*)getNewsFeedData
{
    // Call the webservice
    NSDictionary *resultDictionary = [WebServiceClientHelper makeWebServiceCallAndReturnResult:@"/Mobile/GetNewsFeed" parameters: [[NSMutableDictionary alloc]init]];
    
    // Get the activities
    NSArray *activities = [resultDictionary objectForKey:@"Activities"];
    
    // Create an object for each item and then return
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    for (NSDictionary *activityDictionary in activities)
    {
        [result addObject:[[Activity alloc]initFromDictionary:activityDictionary]];
    }
    
return result;
}

@end
