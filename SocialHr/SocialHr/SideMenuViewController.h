//
//  SideMenuViewController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 8/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuViewController : UITableViewController

@end
