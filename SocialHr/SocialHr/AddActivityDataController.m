//
//  AddActivityDataController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "AddActivityDataController.h"
#import "WebServiceClientHelper.h"

@implementation AddActivityDataController

+(NSArray*)getAvailableActivityTypes:(NSInteger)employeeId
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[NSNumber numberWithInteger:employeeId] forKey:@"employeeId"];
    
    // Call the webservice
    NSDictionary *resultDictionary = [WebServiceClientHelper makeWebServiceCallAndReturnResult:
                                      @"/Mobile/GetActivityTypesThatUserCanAdd" parameters: parameters];
    
    // Get the activities
    NSArray *activities = [resultDictionary objectForKey:@"ActivityTypeNames"];
    return activities;
}

+(void)addActivity:(NSInteger)employeeId activityType:(NSString*)activityType shortDescription:(NSString*)shortDescription content:(NSString*)content  imageData:(NSData*)imageData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[NSNumber numberWithInteger:employeeId] forKey:@"employeeId"];
    [parameters setValue:activityType forKey:@"activityType"];
    [parameters setValue:shortDescription forKey:@"shortDescription"];
    [parameters setValue:content forKey:@"content"];


    if (imageData)
    {
        NSString *imageString = [self newStringInBase64FromData:imageData];
        [parameters setValue:imageString forKey:@"document"];
    }
    
    // Call the webservice
    [WebServiceClientHelper makeWebServiceCallAndReturnResult:
                                      @"/Mobile/AddEmployeeActivity" parameters: parameters];
}


static char base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

+ (NSString *)newStringInBase64FromData:(NSData*)data
{
    NSMutableString *dest = [[NSMutableString alloc] initWithString:@""];
    unsigned char * working = (unsigned char *)[data bytes];
    int srcLen = [data length];
    
    // tackle the source in 3's as conveniently 4 Base64 nibbles fit into 3 bytes
    for (int i=0; i<srcLen; i += 3)
    {
        // for each output nibble
        for (int nib=0; nib<4; nib++)
        {
            // nibble:nib from char:byt
            int byt = (nib == 0)?0:nib-1;
            int ix = (nib+1)*2;
            
            if (i+byt >= srcLen) break;
            
            // extract the top bits of the nibble, if valid
            unsigned char curr = ((working[i+byt] << (8-ix)) & 0x3F);
            
            // extract the bottom bits of the nibble, if valid
            if (i+nib < srcLen) curr |= ((working[i+nib] >> ix) & 0x3F);
            
            [dest appendFormat:@"%c", base64[curr]];
        }
    }
    
    return dest;
}



@end
