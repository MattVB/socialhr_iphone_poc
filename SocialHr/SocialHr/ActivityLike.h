//
//  ActivityLike.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 7/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//


@interface ActivityLike : NSObject

@property NSString *likerEmployeeName;
@property NSInteger likerEmployeeId;

-(id)initFromDictionary:(NSDictionary*)dictionary;

@end
