//
//  ActivityComment.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 7/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityComment : NSObject

@property NSString *commenterEmployeeName;
@property NSInteger commenterEmployeeId;
@property NSString *comment;
@property NSString *commenterPhotoUrl;

-(id)initFromDictionary:(NSDictionary*)dictionary;

@end
