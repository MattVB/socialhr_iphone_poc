//
//  InitialLoadingViewController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 16/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface InitialLoadingViewController : UIViewController

- (void)doAutoSignIn;

@end
