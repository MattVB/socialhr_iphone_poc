//
//  ActivityLike.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 7/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "ActivityLike.h"
#import "DictionaryToModelHelper.h"

@implementation ActivityLike

@synthesize likerEmployeeId = _likerEmployeeId;
@synthesize  likerEmployeeName = _likerEmployeeName;

-(id)initFromDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    
    if (self)
    {
        self.likerEmployeeName = [DictionaryToModelHelper getStringFromDictionary:dictionary withKey:@"LikerEmployeeName"];
        self.likerEmployeeId = [DictionaryToModelHelper getIntegerFromDictionary:dictionary withKey:@"LikerEmployeeId"];
    }
    
    return self;
}

@end
