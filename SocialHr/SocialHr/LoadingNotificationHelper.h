//
//  LoadingNotificationHelper.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 17/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoadingNotificationHelper : NSObject

extern NSString *const NotificationName;

+(void)postLoadingCompleteNotification:(UIViewController*)fromViewController;

+(NSString*)getNotificationName;

+(void)showHudAndStartAction:(UIView*)view selector:(SEL)selector toTarget:(id)target withObject:(id)argument;

@end
