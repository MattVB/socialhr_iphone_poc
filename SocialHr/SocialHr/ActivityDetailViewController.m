//
//  ActivityDetailViewController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "ActivityDetailViewController.h"
#import "ActivityDetailCell.h"
#import "ImageViewHelper.h"
#import "WebServiceUrlHelper.h"
#import "ImageDetailCell.h"
#import "ActivityActionsCell.h"
#import "LikedByListCell.h"
#import "DisplayCommentCell.h"
#import "ActivityComment.h"
#import "ActivityLike.h"
#import "ActivityDataController.h"
#import "ShowImageDocumentViewController.h"
#import "EmployeeLinkHelper.h"

@interface ActivityDetailViewController ()

@end

@implementation ActivityDetailViewController

@synthesize activity = _activity;
@synthesize  cells = _cells;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Set the navigation title
    if (![self.activity.shortDescription isEqualToString:@""])
    {
        self.navigationItem.title = self.activity.shortDescription;
    }
    else
    {
        self.navigationItem.title = @"Feedback";
    }
}



-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    self.cells = [[NSMutableArray alloc]init];
    
    [self.cells addObject:@"Detail"];
    
    
    if (self.activity.imageDocumentUrl)
    {
        [self.cells addObject:@"Image"];
    }
    
    if (self.activity.isLikeable || self.activity.isCommentable)
    {
        [self.cells addObject:@"Actions"];
    }
    
    if (self.activity.likeCount > 0)
    {
        [self.cells addObject:@"LikedByList"];
    }
    
    for (ActivityComment *comment in self.activity.comments) {
        [self.cells addObject:comment];
    }
    
    return self.cells.count;
}

-(void)moveLabelDown:(UILabel*)label downBy:(CGFloat)moveDownBy
{
     label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y + moveDownBy, label.frame.size.width, label.frame.size.height);
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellType = @"DisplayComment";
    
    id object = [self.cells objectAtIndex:indexPath.row];
    
    
    
    if ([object isKindOfClass:[NSString class]])
         {
             cellType = object;
         }
    
    if ([cellType isEqualToString:@"Detail"])
    {
    ActivityDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Detail" forIndexPath:indexPath];
    
    // Populate the labels
        // Add in the employee name label as a link
        [EmployeeLinkHelper populateLabel:cell.labelEmployeeName withLinkForEmployeeWithId:self.activity.employeeId andEmployeeName:self.activity.employeeName withDelegate:self];
        
    cell.labelShortDescription.text = self.activity.shortDescription;
    cell.labelDate.text = self.activity.formattedDate;
    cell.labelContent.text = self.activity.content;
        
        // Resize the labels
        [cell.labelContent sizeToFit];
        [cell.labelShortDescription sizeToFit];
        
        // Move the other components down if necessary
        if (cell.labelShortDescription.frame.size.height > 20)
        {
            CGFloat moveDownBy = cell.labelShortDescription.frame.size.height - 20;
            
            [self moveLabelDown:cell.labelDate downBy:moveDownBy];
            [self moveLabelDown:cell.labelContent downBy:moveDownBy];
        }

    
    // Include the added by if required
    if ([self.activity.employeeName isEqualToString:self.activity.addedByEmployeeName])
    {
        cell.labelAddedBy.text = @"";
    }
    else
    {
        [EmployeeLinkHelper populateLabel:cell.labelAddedBy withLinkForEmployeeWithId:self.activity.addedByEmployeeId andEmployeeName:self.activity.addedByEmployeeName withDelegate:self withSuffix:@":"];
    }
    
    
    // Clear the images
    cell.imageEmployee.image = nil;
    cell.imageActivityType.image =  nil;
    
    // Populate the images
    [ImageViewHelper downloadImageInImageView:cell.imageEmployee withUrlString:[WebServiceUrlHelper createFullUrlForResource:self.activity.employeePhotoUrl]];
    [ImageViewHelper downloadImageInImageView:cell.imageActivityType withUrlString:[WebServiceUrlHelper createFullUrlForResource:self.activity.employeeActivityTypeIconUrl]];
    
    
    return cell;
    }
    
    if ([cellType isEqualToString:@"Image"])
    {
        // Create the cell and populate the image
        ImageDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Images" forIndexPath:indexPath];
        [ImageViewHelper downloadImageInImageView:cell.image withUrlString:[WebServiceUrlHelper createFullUrlForResource:self.activity.imageDocumentUrl]];
        
        // Add tap gesture recogniser so that user can click on the image to view
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleImageTap:)];
        tap.cancelsTouchesInView = YES;
        tap.numberOfTapsRequired = 1;
        [cell.image addGestureRecognizer:tap];
        
        return cell;
    }
    
    if ([cellType isEqualToString:@"Actions"])
    {
        ActivityActionsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Actions" forIndexPath:indexPath];
        
        cell.buttonLike.hidden = !self.activity.isLikeable;
        cell.buttonComment.hidden = !self.activity.isCommentable;
        
        return cell;
    }
    
    if ([cellType isEqualToString:@"LikedByList"])
    {
        LikedByListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LikedByList" forIndexPath:indexPath];
        
        // Populate the links for the liker employees
        [EmployeeLinkHelper populateLabel:cell.labelLikedBy withLikerEmployees:self.activity.likes withDelegate:self];
        
        return cell;
    }
    
    ActivityComment *comment = [self.cells objectAtIndex:indexPath.row];
    
    DisplayCommentCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DisplayComment" forIndexPath:indexPath];
    
    // Set the comment label and then adjust size
    cell.labelComment.text = comment.comment;
    [cell.labelComment sizeToFit];
    
    // Add in the commenter name label as a link
    [EmployeeLinkHelper populateLabel:cell.labelCommenterName withLinkForEmployeeWithId:comment.commenterEmployeeId     andEmployeeName:comment.commenterEmployeeName withDelegate:self withSuffix:@":"];
    
    // Add in the photo
    [ImageViewHelper downloadImageInImageView:cell.imageCommenter withUrlString:[WebServiceUrlHelper createFullUrlForResource:comment.commenterPhotoUrl]];
      
    return cell;
}


- (void) handleImageTap:(UIGestureRecognizer *)gestureRecognizer
{
    [self performSegueWithIdentifier:@"showImage" sender:nil];
}


- (IBAction)like:(id)sender
{
    // Like the activity
    [ActivityDataController likeActivity:self.activity.id];
    
    // Get the updated activity
    self.activity = [ActivityDataController getActivity:self.activity.id];
    
    // Reload the view
    [self.collectionView reloadData];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"addComment"]) {
        AddCommentViewController *viewController = [segue destinationViewController];
        viewController.delegate = self;
        viewController.addCommentTitle = self.activity.shortDescription;
    }
    
    if ([[segue identifier] isEqualToString:@"showImage"]) {
        ShowImageDocumentViewController *viewController = [segue destinationViewController];
        viewController.imageUrl = [WebServiceUrlHelper createFullUrlForResource:self.activity.imageDocumentUrl];
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    // Call the helper to handle the request
    [EmployeeLinkHelper handleLinkToEmployeePressed:url fromViewController:self];
}

-(void)addCommentControllerDidFinish:(AddCommentViewController *)controller comment:(NSString *)comment
{
    // Add the comment
    [ActivityDataController commentOnActivity:self.activity.id comment:comment];
    
    // Dismiss the view
    [controller.navigationController popViewControllerAnimated:YES];
    
    // Get the updated activity
    self.activity = [ActivityDataController getActivity:self.activity.id];
    
    // Reload the view
    [self.collectionView reloadData];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellType = @"DisplayComment";
    
    id object = [self.cells objectAtIndex:indexPath.row];
    
    if ([object isKindOfClass:[NSString class]])
    {
        cellType = object;
    }
    
    CGFloat width = 290;
    
    if ([cellType isEqualToString:@"Detail"])
    {
        return [self getHeightForDetailCellAtIndexPath:indexPath];
    }
    
    if ([cellType isEqualToString:@"Image"])
    {
        return CGSizeMake(width, 230);
    }
    
    if ([cellType isEqualToString:@"Actions"])
    {
        return CGSizeMake(width, 62);
    }
    
    if ([cellType isEqualToString:@"LikedByList"])
    {
        return CGSizeMake(width, 57);
    }
    
    // Get the comment
    ActivityComment *comment = [self.cells objectAtIndex:indexPath.row];
    
    // Need to determine the size of the label
    CGSize maximumLabelSize = CGSizeMake(200,9999);
    CGSize expectedLabelSize = [comment.comment sizeWithFont:[UIFont systemFontOfSize:14]
                                      constrainedToSize:maximumLabelSize];
    
    // Apply a minimum size
    if (expectedLabelSize.height < 40)
    {
        // Minimum height of 64
        return CGSizeMake(width, 64);
    }
    
    // Adjust the size based on the expected height of the comment
    return CGSizeMake(width, expectedLabelSize.height + 30);
}

-(CGSize)getHeightForDetailCellAtIndexPath:(NSIndexPath*)indexPath
{
    CGFloat width = 290;
    
    // Determine the height of the content
    CGSize maximumContentLabelSize = CGSizeMake(220,9999);
    CGSize expectedContentLabelSize = [self.activity.content sizeWithFont:[UIFont systemFontOfSize:14]
                                           constrainedToSize:maximumContentLabelSize];
    
    CGSize maximumDescriptionLabelSize = CGSizeMake(220,9999);
    CGSize expectedDescriptionLabelSize = [self.activity.shortDescription sizeWithFont:[UIFont systemFontOfSize:17]
                                                        constrainedToSize:maximumDescriptionLabelSize];
    
    CGFloat staticHeight = 130;
    
    CGFloat height = staticHeight + expectedContentLabelSize.height + expectedDescriptionLabelSize.height;
    
    return CGSizeMake(width, height);
}
@end
