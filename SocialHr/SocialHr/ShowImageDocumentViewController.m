//
//  ShowImageDocumentViewController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "ShowImageDocumentViewController.h"
#import "ImageViewHelper.h"

@interface ShowImageDocumentViewController ()

@end

@implementation ShowImageDocumentViewController

@synthesize imageUrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [ImageViewHelper downloadImageInImageView:self.image withUrlString:self.imageUrl];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
