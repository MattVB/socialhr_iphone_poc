//
//  AddActivityViewController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectActivityTypeViewController.h"

@protocol AddActivityViewControllerDelegate;

@interface AddActivityViewController : UIViewController <SelectActivityTypeViewControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate, UINavigationControllerDelegate>

@property NSInteger employeeId;
@property (weak, nonatomic) IBOutlet UILabel *labelActivityType;
@property NSArray *availableActivityTypes;
@property (weak, nonatomic) IBOutlet UITextField *textShortDescription;
@property (weak, nonatomic) IBOutlet UITextView *textViewContent;
- (IBAction)done:(id)sender;
- (IBAction)buttonAddPhoto:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imagePhoto;

@property (weak, nonatomic) id <AddActivityViewControllerDelegate> delegate;

@end


@protocol AddActivityViewControllerDelegate <NSObject>
- (void)addActivityComplete:(AddActivityViewController *)controller;
@end