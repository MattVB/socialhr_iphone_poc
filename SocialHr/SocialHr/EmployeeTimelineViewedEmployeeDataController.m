//
//  EmployeeTimelineViewedEmployeeDataController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "EmployeeTimelineViewedEmployeeDataController.h"

@implementation EmployeeTimelineViewedEmployeeDataController

static NSInteger viewedEmployeeId;

+(NSInteger)getEmployeeId
{
    return viewedEmployeeId;
}

+(void)setEmployeeId:(NSInteger)employeeId
{
    viewedEmployeeId = employeeId;
}

@end
