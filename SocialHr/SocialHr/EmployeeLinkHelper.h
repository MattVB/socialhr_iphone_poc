//
//  EmployeeLinkHelper.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 17/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTTAttributedLabel.h"

@interface EmployeeLinkHelper : NSObject

+(void)populateLabel:(UILabel*)label withLinkForEmployeeWithId:(NSInteger)employeeId andEmployeeName:(NSString*)employeeName withDelegate:(NSObject<TTTAttributedLabelDelegate>*) delegate;

+(void)populateLabel:(UILabel*)label withLinkForEmployeeWithId:(NSInteger)employeeId andEmployeeName:(NSString*)employeeName withDelegate:(NSObject<TTTAttributedLabelDelegate>*) delegate withSuffix:(NSString*) suffix;

+(void)handleLinkToEmployeePressed:(NSURL*)linkUrl fromViewController:(UIViewController*)viewController;

+(void)addToEmployeeLinkLabel:(UILabel*)label withLinkForEmployeeWithId:(NSInteger)employeeId andEmployeeName:(NSString*)employeeName withSuffix:(NSString*) suffix;

+(void)populateLabel:(UILabel*)label withLikerEmployees:(NSArray*)employees withDelegate:(NSObject<TTTAttributedLabelDelegate>*) delegate;

@end
