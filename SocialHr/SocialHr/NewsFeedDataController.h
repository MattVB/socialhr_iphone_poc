//
//  NewsFeedDataController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsFeedDataController : NSObject

+(NSMutableArray*)getNewsFeedData;

@end
