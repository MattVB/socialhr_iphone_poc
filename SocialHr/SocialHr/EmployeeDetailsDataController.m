//
//  EmployeeDetailsDataController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 9/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "EmployeeDetailsDataController.h"
#import "WebServiceClientHelper.h"

@implementation EmployeeDetailsDataController

+(EmployeeDetails*)getEmployeeDetails:(NSInteger)employeeId
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[NSNumber numberWithInteger:employeeId] forKey:@"employeeId"];
    
    // Call the webservice
    NSDictionary *resultDictionary = [WebServiceClientHelper makeWebServiceCallAndReturnResult:@"/Mobile/GetEmployeeDetails" parameters: parameters];
    
    EmployeeDetails *result = [[EmployeeDetails alloc]initFromDictionary:resultDictionary];
    
    return result;
}

+(NSArray*)getEmployeesForUser
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    // Call the webservice
    NSDictionary *resultDictionary = [WebServiceClientHelper makeWebServiceCallAndReturnResult:@"/Mobile/GetEmployeesForUser" parameters: parameters];

    NSArray *employees = [resultDictionary objectForKey:@"Employees"];
    
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictionary in employees) {
        [result addObject:[[EmployeeDetails alloc]initFromDictionary:dictionary]];
    }
    
    return result;
}

@end
