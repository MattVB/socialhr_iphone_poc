//
//  MenuViewController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 9/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "EmployeeDetails.h"

@interface MenuViewController : UITableViewController

@property EmployeeDetails *employeeDetails;
@property NSArray *employees;

@end
