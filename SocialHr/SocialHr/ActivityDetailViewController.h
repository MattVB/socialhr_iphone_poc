//
//  ActivityDetailViewController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Activity.h"
#import "AddCommentViewController.h"
#import "TTTAttributedLabel.h"

@interface ActivityDetailViewController : UICollectionViewController <AddCommentViewControllerDelegate, UICollectionViewDelegateFlowLayout, TTTAttributedLabelDelegate>

@property Activity *activity;

@property NSMutableArray *cells;

- (IBAction)like:(id)sender;

@end
