//
//  ActivityDataController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 7/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Activity.h"

@interface ActivityDataController : NSObject

+(void)likeActivity:(NSInteger)activityId;

+(void)commentOnActivity:(NSInteger)activityId comment:(NSString*)comment;

+(Activity*)getActivity:(NSInteger)activityId;

@end
