//
//  AddCommentViewController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 8/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "AddCommentViewController.h"

@interface AddCommentViewController ()

@end

@implementation AddCommentViewController

@synthesize delegate = _delegate;
@synthesize addCommentTitle = _addCommentTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationController.title = self.addCommentTitle;
}

- (IBAction)done:(id)sender
{
    [self.delegate addCommentControllerDidFinish:self comment:self.textAreaComment.text];
}
@end
