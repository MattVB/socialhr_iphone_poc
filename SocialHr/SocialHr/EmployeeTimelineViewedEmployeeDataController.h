//
//  EmployeeTimelineViewedEmployeeDataController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 This is an ugly hack to pass through the ID of the employee who is to be viewed on the employee timeline.
 Used this as an interim measure as due to the combination of sliding menu and UI navigation controller we can't 
 pass the ID through directly to the EmployeeTimelineViewController as we would normally.
 */

@interface EmployeeTimelineViewedEmployeeDataController : NSObject

+(NSInteger)getEmployeeId;

+(void)setEmployeeId:(NSInteger)employeeId;

@end
