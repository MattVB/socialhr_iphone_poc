//
//  EmployeeTimelineViewController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 9/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "EmployeeDetails.h"
#import "AddActivityViewController.h"
#import "TTTAttributedLabel.h"

@interface EmployeeTimelineViewController : UICollectionViewController <AddActivityViewControllerDelegate,UICollectionViewDelegateFlowLayout, TTTAttributedLabelDelegate>
- (IBAction)menu:(id)sender;

@property NSInteger employeeId;
@property EmployeeDetails *employeeDetails;
@property NSArray *activities;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonMenu;

@end
