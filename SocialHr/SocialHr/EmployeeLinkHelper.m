//
//  EmployeeLinkHelper.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 17/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "EmployeeLinkHelper.h"
#import "EmployeeTimelineViewedEmployeeDataController.h"
#import "TTTAttributedLabel.h"
#import "ActivityLike.h"

@implementation EmployeeLinkHelper

+(void)populateLabel:(UILabel*)label withLinkForEmployeeWithId:(NSInteger)employeeId andEmployeeName:(NSString*)employeeName withDelegate:(NSObject<TTTAttributedLabelDelegate>*) delegate
{
    [self populateLabel:label withLinkForEmployeeWithId:employeeId andEmployeeName:employeeName withDelegate:delegate withSuffix:@""];
}

+(void)populateLabel:(UILabel*)label withLinkForEmployeeWithId:(NSInteger)employeeId andEmployeeName:(NSString*)employeeName withDelegate:(NSObject<TTTAttributedLabelDelegate>*) delegate withSuffix:(NSString*) suffix
{
    // Get the attributed label
    TTTAttributedLabel *attributedLabel = [self formatLabel:label withDelegate:delegate];
    
    // Set the text
    attributedLabel.text = [employeeName stringByAppendingString:suffix];
    
    // Get the range
    NSRange range = [attributedLabel.text rangeOfString:employeeName];
    
    // Create the link URL
    NSNumber *employeeIdNumber = [NSNumber numberWithInteger:employeeId];
    [attributedLabel addLinkToURL:[NSURL URLWithString:[@"socialhr://Employee/" stringByAppendingString:[employeeIdNumber stringValue]]] withRange:range]; // Embedding a custom link in a substring
}


+(void)populateLabel:(UILabel*)label withLikerEmployees:(NSArray*)employees withDelegate:(NSObject<TTTAttributedLabelDelegate>*) delegate
{
    // Get the attributed label
    TTTAttributedLabel *attributedLabel = [self formatLabel:label withDelegate:delegate];
    
    NSString *likerList = @"";
    
    // Put the text together
    for (ActivityLike *like in employees)
    {
        if (![likerList isEqualToString:@""])
        {
            likerList = [likerList stringByAppendingString:@", "];
        }
        
        likerList = [likerList stringByAppendingString:like.likerEmployeeName];
    }
    
    // Set the text value
    attributedLabel.text = likerList;

    // add in the links
    for (ActivityLike *like in employees)
    {
        // Get the range
        NSRange range = [attributedLabel.text rangeOfString:like.likerEmployeeName];
        
        // Create the link URL
        NSNumber *employeeIdNumber = [NSNumber numberWithInteger:like.likerEmployeeId];
        [attributedLabel addLinkToURL:[NSURL URLWithString:[@"socialhr://Employee/" stringByAppendingString:[employeeIdNumber stringValue]]] withRange:range]; // Embedding a custom link in a substring
    }
}


+(TTTAttributedLabel*)formatLabel:(UILabel*)label withDelegate:(NSObject<TTTAttributedLabelDelegate>*) delegate
{
    // Get the attributed label
    TTTAttributedLabel *attributedLabel = label;
    
    // Format for the link
    NSMutableDictionary *attributeDictionary = [[NSMutableDictionary alloc]initWithDictionary:attributedLabel.linkAttributes];
    [attributeDictionary setValue:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
    attributedLabel.linkAttributes = attributeDictionary;
    
    
    // Auto detect links and set the delefate
    attributedLabel.dataDetectorTypes = UIDataDetectorTypeLink; // Detect the links
    attributedLabel.delegate = delegate; // Set the delegate

    return attributedLabel;
}

+(void)handleLinkToEmployeePressed:(NSURL *)linkUrl fromViewController:(UIViewController *)viewController
{
    if (![linkUrl.absoluteString hasPrefix:@"socialhr://Employee/"])
    {
        // Link isn't to show an employee
        return;
    }
        // Get the employee ID
        NSString *employeeIdAsString = [linkUrl.absoluteString substringFromIndex:20];
        NSInteger employeeId = [employeeIdAsString integerValue];
        
        // Go to the timeline - set the employee ID first
        [EmployeeTimelineViewedEmployeeDataController setEmployeeId:employeeId];
    
        // Create the view controller and push it onto the stack
        UIViewController *timelineViewController = [viewController.storyboard instantiateViewControllerWithIdentifier:@"EmployeeTimeline"];
        [viewController.navigationController pushViewController:timelineViewController animated:YES];
}

@end
