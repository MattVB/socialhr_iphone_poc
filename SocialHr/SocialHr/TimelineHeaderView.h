//
//  TimelineHeaderView.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 9/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UIImageView *imagePhoto;
@property (weak, nonatomic) IBOutlet UILabel *labelEmployeeName;
@property (weak, nonatomic) IBOutlet UILabel *labelPosition;

@property (weak, nonatomic) IBOutlet UILabel *labelDepartment;
@end
