//
//  UserDetailsDataController.m
//  SocialHr
//
//  Created by Matthew van Boheemen on 6/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import "UserDetailsDataController.h"
#import "WebServiceClientHelper.h"
#import "DictionaryToModelHelper.h"

@implementation UserDetailsDataController

+(NSString*)getBaseUrl
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults stringForKey:@"BaseUrl"];
}

+(NSString*)getUserName
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults stringForKey:@"UserName"];
}

+(NSString*)getPassword
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults stringForKey:@"Password"];
}

+(BOOL)authenticateUser:(NSString *)userName password:(NSString *)password baseUrl:(NSString *)baseUrl
{
    // Update settings
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:baseUrl forKey:@"BaseUrl"];
    [userDefaults setValue:password forKey:@"Password"];
    [userDefaults setValue:userName forKey:@"UserName"];
    
    // Call authentication web service
    return [self callAuthenticationWebService];
}

+(BOOL)doAppStartAutoAuthentication
{
    if ([self getUserName].length == 0 || [self getPassword].length == 0 || [self getBaseUrl].length == 0)
    {
        return NO;
    }
    
    return [self callAuthenticationWebService];
}

+(void)logout
{
    // Clear the password
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:@"" forKey:@"Password"];
}

+(BOOL)callAuthenticationWebService
{
     NSDictionary *resultDictionary = [WebServiceClientHelper makeWebServiceCallAndReturnResult:@"/Mobile/AuthenticateUser" parameters: [[NSMutableDictionary alloc]init]];
    
    BOOL result = [DictionaryToModelHelper getBoolFromDictionary:resultDictionary withKey:@"Success"];
    
    return result;
}

@end
