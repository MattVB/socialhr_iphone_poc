//
//  AddActivityDataController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 10/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddActivityDataController : NSObject

+(NSArray*)getAvailableActivityTypes:(NSInteger)employeeId;

+(void)addActivity:(NSInteger)employeeId activityType:(NSString*)activityType shortDescription:(NSString*)shortDescription content:(NSString*)content imageData:(NSData*)imageData;

@end
