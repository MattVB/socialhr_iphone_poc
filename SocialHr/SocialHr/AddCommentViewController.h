//
//  AddCommentViewController.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 8/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddCommentViewControllerDelegate;


@interface AddCommentViewController : UIViewController

@property (weak, nonatomic) id <AddCommentViewControllerDelegate> delegate;
@property NSString *addCommentTitle;
@property (weak, nonatomic) IBOutlet UITextView *textAreaComment;
- (IBAction)done:(id)sender;

@end


@protocol AddCommentViewControllerDelegate <NSObject>
- (void)addCommentControllerDidFinish:(AddCommentViewController *)controller comment:(NSString *)comment;
@end


