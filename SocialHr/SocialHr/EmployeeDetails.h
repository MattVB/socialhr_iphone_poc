//
//  EmployeeDetails.h
//  SocialHr
//
//  Created by Matthew van Boheemen on 9/01/13.
//  Copyright (c) 2013 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmployeeDetails : NSObject

@property NSInteger id;
@property NSString *employeeName;
@property NSString *department;
@property NSString *position;
@property NSString *employeePhotoUrl;

-(id)initFromDictionary:(NSDictionary*)dictionary;

@end
